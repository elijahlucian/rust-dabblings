# Missile Command

# todo:

## Release 1

https://elijahlucian.itch.io/dank-defense-theyre-coming

### Graphics

- [x] missile smoke

### audio

- [x] BGM - Main Theme
- [x] sfx - projectile explode
- [x] sfx - projectile hit
- [x] sfx - game over

## Release 2

### GamePlay

- [] ammo count
- [] reloading
- [] increase missile speed?
- [] waves
- [] high scores
- [] weapon upgrades

### audio

- [] BGM - Game Over!
- [] sfx - missile incoming
- [] sfx - turret fire
- [] sfx - turret reload

## Relase Someday

- [] Game Mode: Rush - slight break in gameplay, then all the missiles come in at once with extra stuff like mirvs and whatnot. - music changes from chill to hype for main theme
