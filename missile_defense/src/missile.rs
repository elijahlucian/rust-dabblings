use rand::Rng;

#[derive(Debug)]
pub struct Missile {
  pub x: f32,
  pub y: f32,
  id: u8,
  max_distance: f32,
  vector_x: f32,
  vector_y: f32,
  speed: f32,
  death_timer: f32,
  pub in_play: bool,
  pub destroyed: bool,
}

impl Missile {
  pub fn new(id: u8, speed: f32) -> Missile {
    Missile {
      id,
      in_play: false,
      destroyed: false,
      x: 0.0,
      y: 0.0,
      death_timer: 1.0,
      vector_x: 0.0,
      vector_y: 0.0,
      max_distance: 0.0,
      speed,
    }
  }

  pub fn spawn(&mut self, width: f32, height: f32) {
    // fuck this angle bullshit just use vectors

    let mut rng = rand::thread_rng();
    let origin_x = rng.gen_range(0.0, width);
    let dest_x = rng.gen_range(0.0, width);

    let x_diff = dest_x - origin_x;
    let y_diff = height;
    let hyp = x_diff.hypot(y_diff);

    self.vector_x = x_diff / hyp;
    self.vector_y = y_diff / hyp;
    self.max_distance = hyp;
    self.x = origin_x;
    self.y = 0.0;
    self.in_play = true;

    // TEST coords = x: 5.0 & y: 0.0 // for tests should return coords theta of 1.5818914
  }

  pub fn update(&mut self, game_speed: f32, delta: f32) {
    let speed = self.speed * game_speed * delta;
    self.x += self.vector_x * speed;
    self.y += self.vector_y * speed;
  }

  // pub fn show(&self) {}
  pub fn _status(&self) -> String {
    format!("Missile #{} Coords: {}, {}", self.id, self.x, self.y,)
  }
}
