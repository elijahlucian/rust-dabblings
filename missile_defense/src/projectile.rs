#[derive(Debug)]
pub struct Projectile {
  pub x: f32,
  pub y: f32,
  vx: f32,
  vy: f32,
  magnitude: f32,
  distance: f32,
  pub exploded: bool,
  speed: f32,
}

impl Projectile {
  pub fn new(width: f32, height: f32, dest_x: f32, dest_y: f32, speed: f32) -> Projectile {
    let x_diff = dest_x - width / 2.0;
    let y_diff = dest_y - height;
    let hyp = x_diff.hypot(y_diff);
    let vx = x_diff / hyp;
    let vy = y_diff / hyp;
    let magnitude = vx.hypot(vy);

    Projectile {
      x: width / 2.0,
      y: height,
      vx,
      vy,
      magnitude,
      distance: hyp,
      exploded: false,
      speed,
    }
  }

  pub fn update(&mut self, game_speed: f32, delta: f32) {
    if self.exploded {
      return;
    }

    let speed = self.speed * game_speed * delta;
    self.x += self.vx * speed;
    self.y += self.vy * speed;
    self.distance -= self.magnitude * speed;
    if self.distance < 0.0 {
      self.exploded = true;
    }
  }
}
