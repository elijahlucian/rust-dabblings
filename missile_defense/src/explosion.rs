#[derive(Debug)]
pub struct Explosion {
  pub x: f32,
  pub y: f32,
  pub size: f32,
  pub life: u32,
  max_life: u32,
  pub done: bool,
}

impl Explosion {
  pub fn new(x: f32, y: f32, size: f32) -> Explosion {
    Explosion {
      x,
      y,
      size,
      life: 1,
      max_life: 20, // frames
      done: false,
    }
  }
  pub fn update(&mut self, _game_speed: f32, delta: f32) {
    self.life += 1;
    let scaled_life = self.life as f32 * delta;
    if scaled_life > self.max_life as f32 {
      self.done = true;
    }
    self.size = scaled_life as f32 / self.max_life as f32;
  }
}
