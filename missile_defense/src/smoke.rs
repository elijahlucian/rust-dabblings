#[derive(Debug)]
pub struct Smoke {
  pub x: f32,
  pub y: f32,
  angle: f32,
  speed: f32,
  pub life: f32,
  pub done: bool,
}

impl Smoke {
  pub fn new(x: f32, y: f32, angle: f32, speed: f32) -> Smoke {
    Smoke {
      x,
      y,
      angle,
      speed,
      life: 1.0,
      done: false,
    }
  }

  pub fn update(&mut self, game_speed: f32, game_rate: f32) {
    self.life -= 0.01 * game_speed * game_rate;
    let speed = game_speed * game_rate * 0.1 * self.speed;
    if self.life <= 0.0 {
      self.done = true;
      return;
    }
    self.x += (self.angle).sin() * speed;
    self.y += (self.angle).cos() * speed;
  }
}
