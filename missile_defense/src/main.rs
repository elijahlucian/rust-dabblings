// crates
use std::{fs, path::{Path}};
use ggez::conf;
use ggez::event::{self, EventHandler, KeyCode, KeyMods, MouseButton};
use ggez::mint::Vector2;
use ggez::{
    graphics::{self, Color, DrawMode, DrawParam, Font, Mesh, Rect, Scale, Text},
    audio::{Source, SoundSource},
    nalgebra::Point2,
    timer, Context, ContextBuilder, GameResult, audio,
};
use rand::Rng;
use rand::seq::{SliceRandom};

// constants
use std::f32::consts::PI;
const TAU: f32 = PI * 2.0;

// modules
mod explosion;
mod game_const;
mod missile;
mod projectile;
mod smoke;
mod turret;

use crate::explosion::Explosion;
use crate::missile::Missile;
use crate::projectile::Projectile;
use crate::smoke::Smoke;
use crate::turret::Turret;


fn main() -> GameResult {
    let window_setup = conf::WindowSetup {
        title: "🚀 Dank Defense".to_owned(),
        icon: "".to_owned(),
        samples: conf::NumSamples::One,
        srgb: true,
        vsync: true,
    };

    let (mut ctx, mut event_loop) = ContextBuilder::new("dank_defense", "elijah_lucian")
        .add_resource_path("resources")
        .window_setup(window_setup)
        .build()?;

    // ctx.window_setup()?;

    // let conf = conf::Conf {
    //     window_mode: conf::WindowMode::default(),
    //     window_setup: ,
    //     backend: conf::Backend::OpenGL(3,2),
    // };

    let mut game = Game::new(&mut ctx, game_const::WIDTH, game_const::HEIGHT);
    for _i in 0..game_const::MISSILE_STARTING_COUNT {
        game.new_missile()?;
    }

    match event::run(&mut ctx, &mut event_loop, &mut game) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e),
    }

    println!("{:?}", game);
    Ok(())
}

struct _Assets {
    bgm: audio::Source,
    sfx_incoming_missile: audio::Source,
    sfx_turret_fire: audio::Source,
    sfx_projectile_explosion: audio::Source,
    sfx_missile_hit: audio::Source,
}

#[derive(Debug)]
pub struct Game {
    // CLIENT
    width: u32,
    height: u32,
    mouse_down: bool,
    mouse_x: f32,
    mouse_y: f32,
        
    // ASSET RELATED
    small_font: f32,
    med_font: f32,
    large_font: f32,
    font: Font,
    bgm: audio::Source,
    sfx_explosions: Vec<Source>,
    sfx_score: Vec<Source>,
    sfx_game_over: Vec<Source>,

    // GAME LOGIC
    game_speed_multiplier: f32,
    game_started: bool,
    game_over: bool,
    next_missile: f64,
    score: u32,
    time: f64,
    cooldown: f64,
    new_missile: bool,

    // GAME OBJECTS
    missiles: Vec<Missile>,
    projectiles: Vec<Projectile>,
    explosions: Vec<Explosion>,
    smokes: Vec<Smoke>,
    turret: Turret,

    // RENDER MESHES
    missile_mesh: Mesh,
    projectile_mesh: Mesh,
    explosion_mesh: Mesh,
    smoke_mesh: Mesh,
}

impl Game {
    pub fn new(ctx: &mut Context, width: u32, height: u32) -> Game {
        let turret_mesh = Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(0.0, 0.0, 75.0, 50.0),
            Color::from((30, 175, 50)),
        )
        .expect("Could not draw turret");

        let turret = Turret::new(game_const::WIDTH, game_const::HEIGHT, turret_mesh);
        let font = Font::new(ctx, "/rockledge.ttf").unwrap();
        let bgm = audio::Source::new(ctx, "/dank-defense-main-theme.ogg").expect("Audio Did Not Load!");
        
        
        let mut sfx_explosions = Vec::new();
        let mut sfx_score = Vec::new();
        let mut sfx_game_over = Vec::new();
        
        // explosions
        let paths = fs::read_dir("./resources/sfx/explosion").expect("could not find directory");
        for path in paths {
            let sfx = audio::Source::new(
                ctx, Path::new("/sfx/explosion").join(path.unwrap().file_name())
            ).expect("sound not loaded");
            sfx_explosions.push(sfx);
        }

        let paths = fs::read_dir("./resources/sfx/score").expect("could not find directory");
        for path in paths {
            let sfx = audio::Source::new(
                ctx, Path::new("/sfx/score").join(path.unwrap().file_name())
            ).expect("sound not loaded");
            sfx_score.push(sfx);
        }
        
        let paths = fs::read_dir("./resources/sfx/game_over").expect("could not find directory");
        for path in paths {
            let sfx = audio::Source::new(
                ctx, Path::new("/sfx/game_over").join(path.unwrap().file_name())
            ).expect("sound not loaded");
            sfx_game_over.push(sfx);
        }

        let smoke_mesh = Mesh::new_circle(
            ctx,
            DrawMode::fill(),
            Point2::new(0.0, 0.0),
            3.0,
            4.0,
            Color::from((255, 255, 255)),
        )
        .expect("could not create smoke mesh");

        let missile_mesh = Mesh::new_circle(
            ctx,
            DrawMode::fill(),
            Point2::new(0.0, 0.0),
            0.25,
            0.2,
            Color::from((75, 55, 175)),
        )
        .expect("could not create missile mesh");

        let projectile_mesh = Mesh::new_circle(
            ctx,
            DrawMode::fill(),
            Point2::new(0.0, 0.0),
            5.0,
            1.0,
            Color::from((250, 250, 250)),
        )
        .expect("could not create projectile mesh");

        let explosion_mesh = Mesh::new_circle(
            ctx,
            DrawMode::fill(),
            Point2::new(0.0, 0.0),
            game_const::BLAST_RADIUS,
            4.0,
            Color::from((250, 50, 0)),
        )
        .expect("could not create explosion mesh");

        Game {
            font,
            game_speed_multiplier: 1.0,
            game_started: false,
            game_over: false,
            score: 0,
            width,
            bgm,
            height,
            time: 0.0,
            cooldown: 0.0,
            next_missile: game_const::NEW_MISSILE_INTERVAL,
            mouse_down: false,
            mouse_x: 0.0,
            mouse_y: 0.0,
            new_missile: false,
            missiles: vec![],
            projectiles: vec![],
            explosions: vec![],
            smokes: vec![],
            sfx_explosions,
            sfx_score,
            sfx_game_over,
            missile_mesh,
            projectile_mesh,
            explosion_mesh,
            smoke_mesh,
            turret,
            small_font: height as f32 / 20.0,
            med_font: height as f32 / 15.0,
            large_font: height as f32 / 6.0,
        }
    }

    pub fn new_missile(&mut self) -> GameResult<()> {
        let mut rng = rand::thread_rng();
        let speed = rng.gen_range(0.9, 1.1);
        let id = self.missiles.len() as u8;
        let mut missile = Missile::new(id + 1, speed);
        missile.spawn(self.width as f32, self.height as f32);
        self.missiles.push(missile);
        Ok(())
    }

    pub fn spawn_missiles(&mut self) {
        for missile in self.missiles.iter_mut() {
            if !missile.in_play {}
        }
    }

    pub fn new_projectile(&mut self) -> GameResult<()> {
        self.projectiles.push(Projectile::new(
            self.width as f32,
            self.height as f32,
            self.mouse_x,
            self.mouse_y,
            game_const::BASE_PROJECTILE_SPEED,
        ));
        // add projctile to game
        Ok(())
    }

    pub fn new_explosion(&mut self, x: f32, y: f32) -> GameResult<()> {
        let explosion = Explosion::new(x, y, game_const::BLAST_RADIUS);
        self.explosions.push(explosion);
        Ok(())
    }

    pub fn new_smoke(&mut self, x: f32, y: f32) -> GameResult<()> {
        let mut rng = rand::thread_rng();
        let r: f32 = rng.gen();
        let speed: f32 = rng.gen_range(0.3, 1.2);
        let angle = r * TAU;
        let smoke = Smoke::new(x, y, angle, speed);
        self.smokes.push(smoke);
        Ok(())
    }

    fn reset(&mut self) {
        self.score = 0;
        self.game_over = false;
        self.next_missile = game_const::NEW_MISSILE_INTERVAL;
        self.time = 0.0;
    }

    fn clear_game_objects(&mut self) {
        self.missiles = vec![];
        self.projectiles = vec![];
        self.explosions = vec![];
        self.smokes = vec![]
    }

    // fn check_collision(projectile: &Projectile, missile: &Missile) {}

    fn draw_text(
        &mut self,
        ctx: &mut Context,
        content: &str,
        size: f32,
        x: f32,
        y: f32,
        centered: bool,
        shadow: bool,
    ) -> GameResult {
        let font_scale = Scale::uniform(size);
        let mut text = Text::new(content);
        text.set_font(self.font, font_scale);
        let w_offset = text.width(ctx) as f32 / 2.0;
        let draw_params =
            DrawParam::default().dest(Point2::new(x - if centered { w_offset } else { 0.0 }, y));
        graphics::draw(ctx, &text, draw_params)?;

        if shadow {
            let draw_params = DrawParam::default().dest(Point2::new(
                0.05 * size + x - if centered { w_offset } else { 0.0 },
                0.05 * size + y,
            ));
            draw_params.color(Color {
                r: 0.0,
                g: 0.0,
                b: 0.0,
                a: 0.1,
            });
        }

        Ok(())
    }
}

impl EventHandler for Game {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let game_speed = game_const::GAME_SPEED + self.score as f32 / 20.0;
        let dt = timer::delta(ctx).subsec_nanos() as f64 * 1e-9;
        let game_rate = 60.0 / timer::fps(ctx) as f32;
        self.cooldown += dt;

        let mut rng = rand::thread_rng();
        
        if !self.bgm.playing() { 
            self.bgm.set_volume(0.5);
            self.bgm.play()?; 
        }

        if !self.game_started {
            return Ok(());
        }

        self.time += dt;

        if self.missiles.len() < game_const::MISSILE_STARTING_COUNT as usize {
            self.new_missile()?;
        }

        if self.new_missile {
            self.new_missile()?;
            self.new_missile = false;
        }

        if self.mouse_down {
            self.new_projectile()?;
            self.mouse_down = false;
        }

        if self.time > self.next_missile {
            self.new_missile()?;
            self.next_missile += game_const::NEW_MISSILE_INTERVAL;
        }

        let mut new_explosions = Vec::new();

        for projectile in self.projectiles.iter_mut() {
            projectile.update(game_speed, game_rate);

            if projectile.exploded {
                let sfx = self.sfx_explosions.choose_mut(&mut rng).expect("cannot find sfx");
                let pitch = rng.gen_range(0.9,1.1);
                let volume = rng.gen_range(0.18,0.28);
                sfx.set_pitch(pitch);
                sfx.set_volume(volume);
                sfx.play()?;
                new_explosions.push((projectile.x, projectile.y));
                for missile in self.missiles.iter_mut() {        
                    let did_hit = (projectile.x - missile.x).hypot(projectile.y - missile.y)
                        < game_const::BLAST_RADIUS;
                    if did_hit {
                        missile.spawn(self.width as f32, self.height as f32);
                        self.score += 1;
                        let sfx = self.sfx_score.choose_mut(&mut rng).expect("cannot find sfx");
                        let volume = rng.gen_range(0.28,0.38);
                        sfx.set_volume(volume);
                        sfx.play()?;
                    }
                }
            }                
        }

        for (x,y) in new_explosions.iter() {
            self.new_explosion(*x,*y)?;
        }

        for explosion in self.explosions.iter_mut() {
            explosion.update(game_speed, game_rate);
        }

        for i in 0..self.missiles.len() {
            let x = self.missiles[i].x;
            let y = self.missiles[i].y;
            if y > self.height as f32 {
                let sfx = self.sfx_game_over.choose_mut(&mut rng).expect("cannot find sfx");
                sfx.set_volume(0.6);
                sfx.play()?;
                self.game_over = true;
                self.cooldown = 0.0;
            }
            self.new_smoke(x, y)?;
        }

        for m in self.missiles.iter_mut() {
            m.update(game_speed, game_rate);
        }
      
        for smoke in self.smokes.iter_mut() {
            smoke.update(game_speed, game_rate);
        }

        self.explosions.retain(|x| !x.done );
        self.projectiles.retain(|p| !p.exploded);
        self.smokes.retain(|s| !s.done );

        if self.game_over {
            self.clear_game_objects();
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let run_time = timer::time_since_start(ctx).as_millis() as f32 / 1000.0;
        let mid_width = (self.width / 2) as f32;
        let mid_height = (self.height / 2) as f32;

        if !self.game_started {
            graphics::clear(
                ctx,
                Color {
                    r: run_time.sin() / 3.0 + 0.3,
                    g: (run_time / PI).sin() / 3.0 + 0.3,
                    b: (run_time * PI * 2.0).sin() / 3.0 + 0.3,
                    a: 1.0,
                },
            );
            self.draw_text(
                ctx,
                "Click Mouse & Defend!",
                self.med_font,
                mid_width + run_time.sin() * mid_width / 200.0,
                mid_height - (2.0 * (run_time * PI).sin() * mid_height / 50.0).abs(),
                true,
                true,
            )?;

            self.draw_text(
                ctx,
                "DANK DEFENSE",
                self.large_font,
                mid_width,
                mid_height - mid_height / 2.0 + run_time.sin() * mid_width / 200.0,
                true,
                true,
            )?;

            if self.cooldown < 4.0 {
                self.draw_text(
                    ctx,
                    &format!("{}", (5.0 - self.cooldown).abs() as i32 ),
                    self.large_font,
                    mid_width,
                    mid_height + mid_height / 4.0 + run_time.sin() * mid_width / 200.0,
                    true,
                    true,
                )?;
            }
            
            return graphics::present(ctx);
        }
        if self.game_over {
            graphics::clear(ctx, Color::from((250, 50, 50)));
            self.draw_text(
                ctx,
                "GAME OVER!",
                self.large_font,
                mid_width + run_time.sin() * mid_width / 200.0,
                mid_height
                    - (2.0 * (run_time * PI).sin() * mid_height / 50.0).abs()
                    - mid_height * 0.2,
                true,
                true,
            )?;

            self.draw_text(
                ctx,
                &format!("Your Score: {}", self.score),
                self.med_font,
                mid_width + run_time.sin() * mid_width / 200.0,
                mid_height + mid_height * 0.2,
                true,
                true,
            )?;
            if self.cooldown < 4.0 {
                self.draw_text(
                    ctx,
                    &format!("{}", (5.0 - self.cooldown).abs() as i32 ),
                    self.large_font,
                    mid_width,
                    mid_height + mid_height / 3.0 + run_time.sin() * mid_width / 200.0,
                    true,
                    true,
                )?;
            }
            
            return graphics::present(ctx);
        }

        graphics::clear(ctx, Color::from((50, 50, 50)));
        graphics::draw(
            ctx,
            &self.turret.mesh,
            (Point2::new(self.turret.x, self.turret.y),),
        )?;

        for e in self.explosions.iter_mut() {
            let normal_size = (e.size).min(1.0).max(0.0);
            let scale = (normal_size - 1.2).abs();
            let r = scale;
            let g = e.size;
            let b = scale;
            let a = scale;

            let color = Color { r, g, b, a };

            let draw_params = DrawParam::default()
                .dest(Point2::new(e.x, e.y))
                .rotation(e.size * PI)
                .scale(Vector2 {
                    x: scale,
                    y: scale,
                })
                .color(color);

            graphics::draw(ctx, &self.explosion_mesh, draw_params)?;
        }

        for p in self.projectiles.iter_mut() {
            let draw_params = DrawParam::default().dest(Point2::new(p.x, p.y));
            graphics::draw(ctx, &self.projectile_mesh, draw_params)?;
        }

        for t in self.smokes.iter_mut() {
            let color = Color {
                r: 1.0,
                g: 1.0,
                b: 1.0,
                a: t.life,
            };
            let draw_params = DrawParam::default()
                .dest(Point2::new(t.x, t.y))
                .color(color);
            graphics::draw(ctx, &self.smoke_mesh, draw_params)?;
        }

        for m in self.missiles.iter_mut() {
            graphics::draw(ctx, &self.missile_mesh, (Point2::new(m.x, m.y),))?;
        }

        self.draw_text(
            ctx,
            &format!("Score: {}", self.score),
            self.med_font,
            mid_width * 0.1,
            mid_height * 0.1,
            false,
            false,
        )?;

        self.draw_text(
            ctx,
            &format!("Missile Count: {}", self.missiles.len()),
            self.small_font,
            mid_width * 0.1,
            mid_height * 0.1 * 3.0,
            false,
            false,
        )?;

        graphics::present(ctx)
    }

    fn mouse_motion_event(&mut self, _ctx: &mut Context, x: f32, y: f32, _xrel: f32, _yrel: f32) {
        if self.mouse_down {
            self.mouse_x = x;
            self.mouse_y = y;
        }
        self.turret.update(x, y);
    }

    fn mouse_button_down_event(&mut self, _ctx: &mut Context, _button: MouseButton, x: f32, y: f32) {
        if !self.game_started && self.cooldown > 4.0 {
            self.game_started = true;
            return;
        }
        if self.game_over && self.cooldown > 4.0 {
            self.reset();
            return;
        }
        self.mouse_down = true;
        self.mouse_x = x;
        self.mouse_y = y;
    }

    fn key_down_event(&mut self, _ctx: &mut Context, key: KeyCode, _mods: KeyMods, _: bool) {
        match key {
            KeyCode::Return => {
                if !self.game_started && self.cooldown > 4.0 {
                    self.game_started = true
                }
                if self.game_over && self.cooldown > 4.0 {
                    self.reset()
                }
            }
            KeyCode::M => {
                self.new_missile = true;
            }
            _ => (),
        }
    }
}
