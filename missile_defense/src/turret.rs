use ggez::graphics::Mesh;

#[derive(Debug)]
pub struct Turret {
  pub x: f32,
  pub y: f32,
  width: u32,
  height: u32,
  angle: f32,
  distance: f32,
  pub mesh: Mesh,
}

impl Turret {
  pub fn new(window_width: u32, window_height: u32, mesh: Mesh) -> Turret {
    let width = 50;
    let height = 20;
    Turret {
      x: (window_width / 2 - width / 2) as f32,
      y: (window_height - height) as f32,
      width,
      height,
      angle: 1.5818914, // 90 deg -ish
      distance: 0.0,
      mesh,
    }
  }

  pub fn update(&mut self, dest_x: f32, dest_y: f32) {
    // updates angle and distance by mouse cursor

    let opposite = (self.y - dest_y).abs();
    let distance = ((self.x + self.width as f32 / 2.0) - dest_x)
      .abs()
      .hypot(opposite);
    let angle = (opposite / distance).sin();

    self.distance = distance;
    self.angle = angle;

    // println!("angle: {}, distance: {}", angle * 360.0, distance)
  }
}
