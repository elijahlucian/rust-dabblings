#[derive(Debug)]
pub struct Settings {
  agent_count: u32,
}

impl Settings {
  pub fn new(agent_count: u32) -> Settings {
    Settings { agent_count }
  }

  pub fn show(&self) -> &Settings {
    &self
  }

  pub fn get_agent_count(&self) -> u32 {
    self.agent_count
  }
}
