pub const MISSILE_STARTING_COUNT: u32 = 1;
pub const NEW_MISSILE_INTERVAL: f64 = 5.0;
pub const WIDTH: u32 = 800;
pub const HEIGHT: u32 = 600;
pub const BLAST_RADIUS: f32 = 30.0;
pub const GAME_SPEED: f32 = 1.3;
pub const BASE_PROJECTILE_SPEED: f32 = 2.0; // 2x faster than missile
