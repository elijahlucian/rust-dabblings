use std::fs;

fn main() {
    let paths = fs::read_dir("./resources").expect("could not find directory");
    for path in paths {
        println!("path: {}", path.unwrap().path().display());
    }
}
