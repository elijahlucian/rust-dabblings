use nannou::math;
use nannou::noise::*;
use nannou::prelude::*;

mod agent;
use crate::agent::Agent;
const AGENT_COUNT: u32 = 40;

use std::f32::consts::PI;

fn main() {
    nannou::app(model)
        // .event(event)
        .update(update)
        .simple_window(view)
        .run()
}

struct Model {
    w: f32,
    h: f32,
    perlin: Perlin,
    pub t: f32,
    agents: Vec<Agent>,
    agent_size: f32,
}

fn model(app: &App) -> Model {
    // data model
    let square_window_size = 720.0;

    app.main_window()
        .set_inner_size_points(square_window_size, square_window_size);

    let mut agents = vec![];

    let perlin = Perlin::new();

    for j in 0..AGENT_COUNT {
        let v = normalize(j, AGENT_COUNT);
        for i in 0..AGENT_COUNT {
            let u = normalize(i, AGENT_COUNT);

            let index = AGENT_COUNT * j + i;

            let agent = Agent::new(u as f32, v, 5.0, index);

            agents.push(agent);
        }
    }

    let (w, h) = app.main_window().inner_size_pixels();

    let _total_agents = agents.len() as f32;

    Model {
        w: w as f32,
        h: h as f32,
        perlin,
        t: 0.0,
        agents,
        agent_size: square_window_size / AGENT_COUNT as f32,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    // update model 60 times per second.
    let update_rate = 60.0 / app.fps() * 0.1;
    model.t += update_rate;

    for i in 0..model.agents.len() {
        model.agents[i].update(update_rate);
    }
}

fn view(app: &App, model: &Model, frame: &Frame) {
    // render frame
    let draw = app.draw();
    draw.background().hsla(0.0, 0.0, 0.01, 1.0);
    let margin = model.w * 0.15;
    for i in 0..model.agents.len() {
        let agent = &model.agents[i];
        let z = agent.index as f32 / (AGENT_COUNT as f32 * AGENT_COUNT as f32);

        let n = model.perlin.get([
            3.0 * agent.u as f64,
            3.0 * agent.v as f64,
            z as f64,
            model.t as f64 * 0.05,
        ]);

        let x = agent.u * (model.w - margin) / 2.0;
        let y = agent.v * (model.h - margin) / 2.0 + n as f32 * 50.0;

        let n2 = model.perlin.get([n as f64, 0.1 * model.t as f64]);
        let hue = (0.1 * n2 as f32) % 360.0;
        // let size = z * PI * 2.0 + model.t;

        draw.ellipse()
            .resolution(4)
            .hsla(hue, 1.0, 0.5, 1.0)
            .w(3.0 + 3.0 * n2.abs() as f32 * model.agent_size)
            .h(3.0 + 3.0 * n.abs() as f32 * model.agent_size)
            .x_y(x, y);
    }
    draw.to_frame(app, &frame).unwrap();
}

fn normalize(i: u32, total: u32) -> f32 {
    let half: i32 = total as i32 / 2;
    ((i as i32 - (half)) as f32 / half as f32)
}
