
#[derive(Debug)]
pub struct Agent {
    pub u: f32,
    pub v: f32,
    pub size: f32,
    pub index: u32,
}

impl Agent {
  pub fn new(u: f32, v: f32, size: f32, index: u32) -> Agent {
    Agent {
      u,v,size,index,
    }
  }

  pub fn update(&self, _dt: f32) {

  }
}