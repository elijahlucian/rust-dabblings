use std::sync::{Arc, Mutex};
use warp::*;

#[tokio::main]
async fn main() {
    println!("Hello, world!");

    // let routes = warp::path("hello").map(|| "Hello, world!");
    let count = Arc::new(Mutex::new(0));

    let visits = warp::path("visits").map({
        let c = count.clone();
        *c.lock().unwrap() += 1;
        move || format!("{}", *c.lock().unwrap())
    });

    let hello = warp::path!("hello" / String).map(|name| {
        // what
        format!("Hello, {}!", name)
    });

    let routes = warp::get().and(hello).or(visits);

    println!("Listening on localhost:3030");
    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}
