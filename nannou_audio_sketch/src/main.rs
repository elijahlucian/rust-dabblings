use nannou::prelude::*;
use nannou_audio as audio;
use nannou_audio::Buffer;
use std::f64::consts::PI;

fn main() {
    nannou::app(model)
        .update(update)
        .view(view)
        .run();
}

struct Model {
    w: u32,
    h: u32,
    t: f32,
    stream: audio::Stream<Audio>,
}

struct Audio {
    phase: f64,
    hz: f64,
}

fn model(app: &App) -> Model {
    let square_window_size = 720.0;

    app.new_window()
        .key_pressed(key_pressed)
        .mouse_pressed(mouse_pressed)
        .mouse_moved(mouse_moved)
        .build()
        .unwrap();

    app.main_window().set_inner_size_points(square_window_size, square_window_size);

    let (w, h) = app.main_window().inner_size_pixels();


    let audio_host = audio::Host::new();
    
    let model = Audio {
        phase: 0.0,
        hz: 440.0,
    };
    let stream = audio_host
        .new_output_stream(model)
        .render(audio)
        .build()
        .unwrap();

    Model { 
        w,
        h,
        t: 0.0,
        stream 
    }
}

fn audio(audio: &mut Audio, buffer: &mut Buffer) {
    let sample_rate = buffer.sample_rate() as f64;
    let volume = 0.24;

    // println!("number of frames => {:?}", buffer.len());

    for frame in buffer.frames_mut() {
        let sine_amp = (2.0 * PI * audio.phase).sin() as f32;
        audio.phase += audio.hz / sample_rate;
        audio.phase %= sample_rate;

        for channel in frame {
            *channel = sine_amp * volume;
        }

    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let update_rate = 60.0 / app.fps() * 0.1;

    model.t += update_rate * 0.1;
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Space => {
            if model.stream.is_playing() {
                model.stream.pause().unwrap();
            } else {
                model.stream.play().unwrap();
            }
        }
        _ => {}
    }
}


fn mouse_pressed(_app: &App, _model: &mut Model, button: MouseButton) {
    println!("Mouse Button => {:?}", button)
}

fn mouse_moved(_app: &App, model: &mut Model, pos: Point2) {
    // println!("Mouse Position => {:?}", pos);
    println!("Model strea => {:?}", model.stream);
}

fn view(app: &App, model: &Model, frame: &Frame) {
    let draw = app.draw();
    draw.background().hsla(0.0, 0.0, 0.01, 1.0);

    let xb = model.w as f32 / 2.0;
    let yb = model.h as f32 / 2.0;
    let x = model.t.sin() * xb * 0.5;
    let y = model.t.cos() * yb * 0.5;

    draw.ellipse()
        .resolution(10)
        .hsla(120.0, 1.0, 0.5, 1.0)
        .w(10.0)
        .h(10.0)
        .x_y(x,y);

    draw.to_frame(app, &frame).unwrap();
}