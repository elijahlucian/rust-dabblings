# My Dabblings in rust.

If you like rust and want to dabble, join me!

# missile_defense

A little missile defense-esqe game to get my feet wet with rust

# sketches

Some graphical sketches using Nannou

# playground

Just random things and stuff

# TODO

- [] something to do with realtime audio
- [] something to do with realtime video
- [] some IOT thingies?
- [] something totally unordinary
