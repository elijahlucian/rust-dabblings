use cpal;
use cpal::traits::*;
use hound;

fn main() {
    let mut _time: f32 = 0.0;
    let host = cpal::default_host();

    let output_device = host
        .default_output_device()
        .expect("no output device available");
    let input_device = host
        .default_input_device()
        .expect("no input device available");

    let mut supported_output_formats_range = output_device
        .supported_output_formats()
        .expect("error while querying output formats");

    let _output_format = supported_output_formats_range
        .next()
        .expect("no supported format?!")
        .with_max_sample_rate();

    let mut supported_input_formats_range = input_device
        .supported_input_formats()
        .expect("error while querying input formats");

    let input_format = supported_input_formats_range
        .next()
        .expect("no supported format?!")
        .with_max_sample_rate();

    let event_loop = host.
    let stream_id = event_loop
        .build_input_stream(&input_device, &input_format)
        .unwrap();
    event_loop
        .play_stream(stream_id)
        .expect("failed to play stream");

    const PATH: &'static str = concat!(env!("CARGO_MANIFEST_DIR"), "/recorded", ".wav");
    let spec = wav_spec_from_format(&input_format);

    let writer = hound::WavWriter::create(PATH, spec).expect("blah");
    println!("WRITER SPEC => {:?}", writer.spec());
    let writer = std::sync::Arc::new(std::sync::Mutex::new(Some(writer)));

    println!("Starting Recording...");
    let recording = std::sync::Arc::new(std::sync::atomic::AtomicBool::new(true));

    let writer_2 = writer.clone();
    let recording_2 = recording.clone();

    std::thread::spawn(move || {
        event_loop.run(move |id, event| {
            let data = match event {
                Ok(data) => data,
                Err(err) => {
                    eprintln!("an error occurred on stream {:?}: {}", id, err);
                    return;
                }
            };

            if !recording_2.load(std::sync::atomic::Ordering::Relaxed) {
                return;
            }

            match data {
                cpal::StreamData::Input {
                    buffer: cpal::UnknownTypeInputBuffer::F32(buffer),
                } => {
                    if let Ok(mut guard) = writer_2.try_lock() {
                        if let Some(writer) = guard.as_mut() {
                            for &sample in buffer.iter() {
                                writer.write_sample(sample * sample).ok();
                            }
                        }
                    }
                }
                _ => (),
            }
        })
    });

    std::thread::sleep(std::time::Duration::from_secs(3));
    recording.store(false, std::sync::atomic::Ordering::Relaxed);
    writer
        .lock()
        .unwrap()
        .take()
        .unwrap()
        .finalize()
        .expect("WTFFFF");
    println!("Recording {} complete!", PATH);
}

fn sample_format(format: cpal::SampleFormat) -> hound::SampleFormat {
    match format {
        cpal::SampleFormat::U16 => hound::SampleFormat::Int,
        cpal::SampleFormat::I16 => hound::SampleFormat::Int,
        cpal::SampleFormat::F32 => hound::SampleFormat::Float,
    }
}

fn wav_spec_from_format(format: &cpal::Format) -> hound::WavSpec {
    hound::WavSpec {
        channels: format.channels as _,
        sample_rate: format.sample_rate.0 as _,
        bits_per_sample: (format.data_type.sample_size() * 8) as _,
        sample_format: sample_format(format.data_type),
    }
}
