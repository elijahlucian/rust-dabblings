use std::io::prelude::*;
use std::io::BufWriter;
use std::net::TcpStream;

fn main() {
    let stream = TcpStream::connect("localhost:34254").expect("ERR >> couldn't connect");
    let mut buffer = BufWriter::with_capacity(128, stream);

    let blah: [String; 3];

    blah = ["a".to_owned(), "b".to_owned(), "c".to_owned()];

    for msg in blah.iter() {
        println!("WHAT {}", msg);
    }

    for _i in 0..10000 {
        for j in 0..255 {
            buffer.write(&[j]).unwrap();
        }
    }

    println!("thingy written!");
}
